//
//  AppDelegate.swift
//  SocialNetwork
//
//  Created by Le Quang Anh on 1/13/21.
//

import UIKit
import Firebase
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        let vc = LoginViewController()
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        
        return true
    }

}

