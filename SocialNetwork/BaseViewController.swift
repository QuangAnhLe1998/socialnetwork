//
//  BaseViewController.swift
//  SocialNetwork
//
//  Created by Le Quang Anh on 1/13/21.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createEvent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchApi()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.willLayoutSubView()
        
    }
    
    func createEvent(){
        
    }
    
    func fetchApi(){
        
    }
    
    func willLayoutSubView(){
        
    }
    

}
