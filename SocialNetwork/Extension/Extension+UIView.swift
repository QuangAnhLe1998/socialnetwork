//
//  Extension+UIView.swift
//  SocialNetwork
//
//  Created by Le Quang Anh on 1/13/21.
//

import Foundation
import UIKit

extension UIView {
    func addCornerRadius(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
