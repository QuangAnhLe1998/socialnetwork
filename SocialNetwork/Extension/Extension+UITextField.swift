//
//  Extension+UITextField.swift
//  SocialNetwork
//
//  Created by Le Quang Anh on 1/13/21.
//

import Foundation
import UIKit

extension UITextField {
//    func setPasswordToggleImage(_ imageView : UIImageView){
//        if(isSecureTextEntry){
//            imageView.image = UIImage(named: "ic_show_pass")
//        }else{
//            imageView.image = UIImage(named: "ic_user")
//        }
//    }
    
    
    
}

extension UITextField {
    func setPasswordToggleImage(_ imageView : UIImageView){
        isSecureTextEntry = !isSecureTextEntry
        
        imageView.image = isSecureTextEntry ? UIImage(named: "ic_show_pass") : UIImage(named: "ic_user")
        
        if let existingText = text, isSecureTextEntry {
            deleteBackward()
            if let textRange = textRange(from: beginningOfDocument, to: endOfDocument) {
                replace(textRange, withText: existingText)
            }
        }
        
        if let existingSelectedTextRange = selectedTextRange {
            
            selectedTextRange = nil
            selectedTextRange = existingSelectedTextRange
        }
    }
}
