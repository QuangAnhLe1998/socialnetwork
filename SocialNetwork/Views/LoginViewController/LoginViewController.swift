//
//  LoginViewController.swift
//  SocialNetwork
//
//  Created by Le Quang Anh on 1/13/21.
//

import UIKit

class LoginViewController: BaseViewController {
    @IBOutlet var viewUserName: UIView!
    @IBOutlet var viewPassword: UIView!
    @IBOutlet var imgShopPassword: UIImageView!
    @IBOutlet var viewLoginFB: UIView!
    @IBOutlet var viewLoginGoogle: UIView!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var tfUserName: UITextField!
    @IBOutlet var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func createEvent() {
        self.settingTextField()
    }
    
    
    override func willLayoutSubView() {
        viewUserName.addCornerRadius(radius: 10)
        viewPassword.addCornerRadius(radius: 10)
        viewLoginGoogle.addCornerRadius(radius: 10)
        viewLoginFB.addCornerRadius(radius: 10)
        btnSignIn.addCornerRadius(radius: 10)
    }
    
    override func fetchApi() {
        
    }
    @IBAction func btnForgotPassword(_ sender: Any) {
        
    }
    @IBAction func btnSignIn(_ sender: Any) {
        
    }
    @IBAction func btnDontHaveAccount(_ sender: Any) {
    }
    

}

extension LoginViewController {
    func settingTextField(){
        tfUserName.delegate = self
        tfPassword.delegate = self
        imgShopPassword.isUserInteractionEnabled = true
        imgShopPassword.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionImageShowPass)))
    }
    
    @objc func actionImageShowPass(){
        tfPassword.setPasswordToggleImage(imgShopPassword)
    }
    
}


extension LoginViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfUserName.resignFirstResponder()
        tfPassword.resignFirstResponder()
        return true
    }
}
